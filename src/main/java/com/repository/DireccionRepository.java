package com.repository;

import com.entities.Comuna;
import com.entities.Direccion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DireccionRepository extends JpaRepository<Direccion, Integer> {

    List<Direccion> findAllByComuna(Comuna comuna);

}
