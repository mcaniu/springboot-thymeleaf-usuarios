package com.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
@Setter
public class CustomUserDetailsDTO extends User {

    private final Integer id;
    private final String firstName;
    private final String lastName;
    private final String email;

    public CustomUserDetailsDTO(String username, String password, Collection authorities,
                                 Integer id, String firstName, String lastName, String email) {
        super(username, password, authorities);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.id = id;
    }


}
