package com.mappers;

import com.dto.RegionDTO;
import com.entities.Region;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface RegionMapper {
    RegionMapper INSTANCE = Mappers.getMapper(RegionMapper.class);

    @Mapping(source = "id", target = "id")
    RegionDTO objectToDto(Region region);

    @Mapping(source = "id", target = "id")
    Region dtoToObject(RegionDTO regionDTO);

    List<RegionDTO> map(List<Region> estados);

    List<Region> mapDtos(List<RegionDTO> regionesDTOS);

}
