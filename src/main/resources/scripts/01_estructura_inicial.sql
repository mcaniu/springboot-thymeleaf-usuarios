CREATE TABLE IF NOT EXISTS `region`
(
  `id` INTEGER AUTO_INCREMENT PRIMARY KEY,
  `nombre`    VARCHAR(100),
  `activo`    TINYINT
);

CREATE TABLE IF NOT EXISTS `comuna`
(
  `id` INTEGER AUTO_INCREMENT PRIMARY KEY,
  `nombre`    VARCHAR(100),
  `activo`    TINYINT,
  `id_region` INTEGER
    CONSTRAINT FK_region1 REFERENCES region
);

CREATE TABLE IF NOT EXISTS `tipo_accion`
(
  `id` INTEGER AUTO_INCREMENT PRIMARY KEY,
  `nombre`         VARCHAR(50),
  `activo`         TINYINT
);

CREATE TABLE IF NOT EXISTS `direccion`
(
  `id` INTEGER AUTO_INCREMENT PRIMARY KEY,
  `descripcion`  VARCHAR(100),
  `id_comuna`    INTEGER
    CONSTRAINT FK_comuna1 REFERENCES comuna
);

CREATE TABLE IF NOT EXISTS `estado`
(
  `id`   INTEGER AUTO_INCREMENT PRIMARY KEY,
  `descripcion` VARCHAR(50),
  `activo`      TINYINT
);

CREATE TABLE IF NOT EXISTS `tipo_perfil`
(
  `id` INTEGER AUTO_INCREMENT PRIMARY KEY,
  `nombre`         VARCHAR(50),
  `activo`         TINYINT
);

CREATE TABLE IF NOT EXISTS `usuario`
(
  `id`       INTEGER AUTO_INCREMENT PRIMARY KEY,
  `rut`              VARCHAR(15),
  `nombres`          VARCHAR(100),
  `apellido_p`       VARCHAR(50),
  `apellido_m`       VARCHAR(50),
  `fecha_nacimiento` DATETIME,
  `correo`           VARCHAR(50),
  `pass`             VARCHAR(200),
  `activo`           TINYINT,
  `id_estado`        INTEGER
    CONSTRAINT FK_estado1 REFERENCES estado,
  `id_tipo_perfil`   INTEGER
    CONSTRAINT FK_tipo_perfil1 REFERENCES tipo_perfil,
  `id_direccion`     INTEGER NULL
    CONSTRAINT FK_direccion1 REFERENCES direccion
);

CREATE TABLE IF NOT EXISTS `log`
(
  `id`                      INTEGER AUTO_INCREMENT PRIMARY KEY,
  `fecha_modificacion`      DATETIME,
  `nombre_tabla_afectada`          VARCHAR(100),
  `request`            VARCHAR(1000),
  `id_tabla_afectada`    INTEGER,
  `id_usuario_modificacion` INTEGER NULL
    CONSTRAINT FK_usuario1 REFERENCES usuario,
  `id_tipo_accion`          INTEGER CONSTRAINT FK_accion1 REFERENCES tipo_accion,
  `es_exitosa`          TINYINT

);



INSERT INTO `estado`(`descripcion`, `activo`)
VALUES ('BLOQUEADO', 1),
       ('SIN_CONFIRMACION', 1),
       ('DESHABILITADO', 1),
       ('HABILITADO', 1);

INSERT INTO `tipo_perfil`(`nombre`, `activo`)
VALUES ('ROLE_ADMINISTRADOR', 1),
       ('ROLE_USUARIO', 1);

INSERT INTO `region`(`nombre`, `activo`)
VALUES ('Región Metropolitana', 1),
       ('La Araucania', 2);

INSERT INTO `comuna`(`nombre`, `activo`, `id_region`)
VALUES ('Santiago', 1, 1),
       ('La Pintana', 1, 1);

INSERT INTO `direccion`(`descripcion`, `id_comuna`)
VALUES ('Esta es mi direccion lala', 1);

INSERT INTO `usuario`(`rut`, `nombres`, `apellido_m`, `apellido_p`, `fecha_nacimiento`, `correo`, `pass`,
                      `activo`, `id_estado`, `id_tipo_perfil`, `id_direccion`)
VALUES ('18457330-k', 'admin', 'caniu', 'antillanca', sysdate(), 'administrador@hotmail.com', '$2a$10$pU0Nn8LqaPV89KOW4SlkQeet2aAgYhyL7DqFqWkzfDf7mycvqHHz6', 1, 1, 1, 1),
       ('15864850-4', 'user', 'copito', 'copito', sysdate(), 'user@hotmail.com', '$2a$10$tTjrXxo0eqfCmAcKKMVNjuPYGX6q/WzZG48QQD45yufhWJ6ebUigG', 1, 1, 2, 1);
-- values ('publico@hotmail.com', '1', sysdate(), true, 1, 1, 'SYSTEM'),
--        ('administrador@hotmail.com', '1', sysdate(), true, 2, 2, 'SYSTEM'),
--        ('funcionario@hotmail.com', '1', sysdate(), true, 3, 3, 'SYSTEM'),
--        ('funcadmin@hotmail.com', '1', sysdate(), true, 4, 4, 'SYSTEM');


INSERT INTO `tipo_accion`(`nombre`, `activo`)
VALUES ('LOGIN', 1),
       ('INSERT', 1),
       ('UPDATE', 1),
       ('DELETE', 1);

-- INSERT INTO `log`(`fecha_modificacion`, `tabla_afectada`, `data_enviada`, `id_registro_afectado`, `id_tipo_accion`)
-- VALUES (sysdate(), 'estado', 'BLOQUEADO', 1, 3),
--        (sysdate(), 'estado', 'SIN_CONFIRMACION', 2, 3),
--        (sysdate(), 'estado', 'DESHABILITADO', 3, 3),
--        (sysdate(), 'estado', 'HABILITADO', 4, 3),
--
--        (sysdate(), 'tipo_perfil', 'ADMINISTRADOR', 1, 3),
--        (sysdate(), 'tipo_perfil', 'GENERICO', 2, 3),
--
--        (sysdate(), 'region', 'Región Metropolitana', 1, 3),
--        (sysdate(), 'region', 'La Araucania', 2, 3),
--
--        (sysdate(), 'comuna', 'Santiago', 1, 3),
--        (sysdate(), 'comuna', 'La Pintana', 2, 3),
--
--        (sysdate(), 'direccion', 'Esta es mi direccion lala', 1, 3),
--        (sysdate(), 'usuario', '18457330-k', 1, 3);
