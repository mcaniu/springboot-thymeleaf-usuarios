package com.services.impl;


import com.dto.MailDTO;
import com.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Map;

import static com.utils.AppConstant.ENVIRONMENT_PROD;

@Service
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private SpringTemplateEngine thymeleafTemplateEngine;

    @Value("classpath:/templates/mails/logo-mail.png")
    private Resource resourceFile;

    @Value("${spring.mail.host}")
    private String environment;

    @Override
    public void sendMessageUsingThymeleafTemplateWithAttachment(MailDTO mailDTO, Map<String, Object> camposDinamicos, String plantilla) throws MessagingException {
        if (environment.equals(ENVIRONMENT_PROD)) {
            Context thymeleafContext = new Context();

            if (camposDinamicos != null && camposDinamicos.size() > 1) {
                thymeleafContext.setVariables(camposDinamicos);
            }

            String htmlBody = thymeleafTemplateEngine.process("/mails/" + plantilla, thymeleafContext);

            // procesar html generado
            MimeMessage message = emailSender.createMimeMessage();


            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            // helper.setFrom("noreply@gmail.com");
            helper.setTo(mailDTO.getTo());
            helper.setSubject(mailDTO.getSubject());
            helper.setText(htmlBody, true);

            helper.addInline("logo.png", resourceFile);


            if (mailDTO.getFiles() != null) {
                for (MultipartFile attachment : mailDTO.getFiles()) {
                    // Attach each file to the email
                    if (attachment.getOriginalFilename() != null) {
                        helper.addAttachment(attachment.getOriginalFilename(), attachment::getInputStream);
                    }

                }
            }


            emailSender.send(message);
        }

    }


}
