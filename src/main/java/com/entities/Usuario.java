package com.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "USUARIO")
@Getter
@Setter
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "RUT")
    public String rut;

    @Column(name = "NOMBRES")
    public String nombres;

    @Column(name = "APELLIDO_P")
    public String apellidoP;

    @Column(name = "APELLIDO_M")
    public String apellidoM;

    @Column(name = "FECHA_NACIMIENTO")
    public Date fechaNacimiento;

    @Column(name = "CORREO")
    public String correo;

    @Column(name = "PASS")
    public String pass;

    @ManyToOne
    @JoinColumn(name = "ID_ESTADO", referencedColumnName = "ID")
    public Estado estado;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_PERFIL", referencedColumnName = "ID")
    public Perfil perfil;

    @OneToOne
    @JoinColumn(name = "ID_DIRECCION", referencedColumnName = "ID")
    public Direccion direccion;

    @Column(name = "ACTIVO")
    public Boolean activo = true;

}
