package com.utils;

import com.exceptions.UsuarioException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.var;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class AppConstant {

    public static Integer ROLE_ADMINISTRADOR = 1;
    public static Integer ROLE_USUARIO = 2;
    public static Integer SIN_CONFIRMACION = 2;

    public static String TRANSACCION_EXITOSO = "Operacion completada con exito";
    public static String TRANSACCION_FALLIDO = "Ha ocurrido un error al ejecutar accion";
    public static String TRANSACCION_EXITOSO_ALERT = "alert alert-success alert-dismissible fade show";
    public static String TRANSACCION_FALLIDO_ALERT = "alert alert-danger alert-dismissible fade show";
    public static String IDENTIFICADOR_NULO = "Identificador no debe ir vacio o en cero";
    public static String REGISTRO_NO_EXISTENTE = "Ocurrio un problema al consultar el registro";
    public static String DATE_PATTERN1 = "ddMMyyyyhhmmss";
    public static String DATE_PATTERN2 = "dd/MM/yyyy hh:mm:ss";
    public static String DATE_PATTERN3 = "dd/MM/yyyy";
    public static String MAIL_ASUNTO1 = "Notificación de Acceso";
    public static String MAIL_ASUNTO2 = "Notificación de Modificacion Datos";
    public static String MAIL_PLANTILLA1 = "plantilla1.html";

    public static Integer ACCION_LOGIN = 1;
    public static Integer ACCION_CREACION = 2;
    public static Integer ACCION_MODIFICACION = 3;
    public static Integer ACCION_ELIMINACION = 4;

    public static String TABLA_USUARIO = "USUARIO";
    public static String TABLA_COMUNA = "COMUNA";
    public static String TABLA_REGION = "REGION";
    public static String TABLA_PERFIL = "PERFIL";
    public static String TABLA_ESTADO = "ESTADO";
    public static String ENVIRONMENT_PROD = "PROD";


    // Caracteres válidos para la contraseña
    private static final String CARACTERES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public static String dateToString(Date date, String patron) {
        DateFormat dateFormat = new SimpleDateFormat(patron);
        return dateFormat.format(date);

    }

    public static List<String> validationErrors(BindingResult result) {
        List<String> listaErrores = new ArrayList<>();
        for (ObjectError error : result.getAllErrors()) {
            listaErrores.add(error.getDefaultMessage());
        }

        return listaErrores;
    }


    public static void generarExceptionObjetoNulo(Object o) throws UsuarioException {
        if (o == null) {
            throw new UsuarioException("validation", "registro no existente");
        }

    }

    public static Integer extraerId(Integer id) {
        return (id == null) ? 0 : id;
    }

    public static void redirectAttributes(RedirectAttributes redirAttrs, boolean esTransaccionOK) {
        redirAttrs.addFlashAttribute("message", Collections.singletonList((esTransaccionOK) ? TRANSACCION_EXITOSO : TRANSACCION_FALLIDO));
        redirAttrs.addFlashAttribute("alertType", (esTransaccionOK) ? TRANSACCION_EXITOSO_ALERT : TRANSACCION_FALLIDO_ALERT);

    }

    public static <S, T> List<T> mapList(List<S> source, Class<T> targetClass) {
        ModelMapper modelMapper = new ModelMapper();
        return source
                .stream()
                .map(element -> modelMapper.map(element, targetClass))
                .collect(Collectors.toList());
    }

    public static ObjectMapper objectMapper() {
        return new ObjectMapper();
    }


    public static String generarContrasenaAleatoria() {
        SecureRandom random = new SecureRandom();
        StringBuilder contrasena = new StringBuilder();

        // Genera la longitud aleatoria entre 8 y 15 caracteres
        int longitud = random.nextInt(8) + 8;

        for (int i = 0; i < longitud; i++) {
            // Elige un carácter aleatorio de la cadena de caracteres
            int indice = random.nextInt(CARACTERES.length());
            char caracter = CARACTERES.charAt(indice);
            contrasena.append(caracter);
        }

        return contrasena.toString();
    }

//    public static ResponseEntity<Resource> responseFile(String nombreArchivo) {
//        Resource file = descargar(nombreArchivo);
//        return ResponseEntity.ok()
//                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
//    }


    public static boolean validarRut(String rutString) {
        if (StringUtils.isEmpty(rutString)) {
            return false;
        }

        boolean validacion = false;
        String[] split = rutString.trim().split("\\.");
        String[] guion = rutString.trim().split("-");

        if (split.length > 1 || guion.length == 1) {
            return false;
        }

        var rut = rutString.toUpperCase().replace(".", "").replace("-", "");
        int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

        char dv = rut.charAt(rut.length() - 1);

        int m = 0;
        int s = 1;
        for (; rutAux != 0; rutAux /= 10) {
            s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
        }
        if (dv == (char) (s != 0 ? s + 47 : 75)) {
            validacion = true;
        }

        return validacion;
    }

    public static Map<String, Object> respuestaError(String msg, List<String> listadoErroresValidacion) {
        Map<String, Object> res = new HashMap<>();
        res.put("success", false);
        res.put("msg", msg);
        if (listadoErroresValidacion != null && listadoErroresValidacion.size() > 0) {
            res.put("validationErrors", listadoErroresValidacion);
        }
        return res;
    }

}
