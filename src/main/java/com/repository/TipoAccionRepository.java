package com.repository;

import com.entities.TipoAccion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoAccionRepository extends JpaRepository<TipoAccion, Integer> {
}
