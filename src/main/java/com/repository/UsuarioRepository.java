package com.repository;

import com.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

    List<Usuario> findAllByActivoTrue();

    Optional<Usuario> findByCorreoAndActivoTrue(String correo);

    boolean existsByRut(String rut);

    boolean existsByCorreo(String correo);

    boolean existsByIdNotAndRut(Integer id, String rut);

    boolean existsByIdNotAndCorreo(Integer id, String correo);

    Optional<Usuario> findByIdAndActivoTrue(Integer id);

}
