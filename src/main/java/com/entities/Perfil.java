package com.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "TIPO_PERFIL")
@Getter
@Setter
public class Perfil {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "NOMBRE")
    public String nombre;

    @Column(name = "ACTIVO")
    public Boolean activo;

}
