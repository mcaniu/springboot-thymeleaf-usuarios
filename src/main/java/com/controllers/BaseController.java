package com.controllers;

import com.dto.CustomUserDetailsDTO;
import com.services.AutentificacionService;
import com.services.LogService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;

@Log4j2
public abstract class BaseController {

    @Autowired
    private AutentificacionService autentificacionService;

    @Autowired
    @Qualifier("logService1")
    LogService logService;

    protected CustomUserDetailsDTO usuarioConectado() {
        Authentication authentication = autentificacionService.getAuthentication();

        if (authentication.getPrincipal() instanceof CustomUserDetailsDTO) {
            return ((CustomUserDetailsDTO) authentication.getPrincipal());
        } else {
            return null;
        }
    }

    protected void registrarLog(Integer accion, String nombreTabla, Integer idJson, String jsonObject, boolean esTransaccionOK) {
        try {
            CustomUserDetailsDTO customUserDetailsDTO = usuarioConectado();
            if (customUserDetailsDTO == null) {
                log.info("no existe usuario conectado");
            } else {
                logService.registrar(customUserDetailsDTO.getId(), accion, nombreTabla, idJson, jsonObject, esTransaccionOK);
            }
        } catch (Exception e) {
            log.error("registrarLog(): ", e);
        }
    }
}
