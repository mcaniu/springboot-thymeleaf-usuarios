package com.controllers;

import com.dto.ComunaDTO;
import com.dto.UsuarioDTO;
import com.services.ComunaService;
import com.services.EstadoService;
import com.services.RegionService;
import com.services.UsuarioService;
import com.utils.AppConstant;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.utils.AppConstant.*;
import static java.util.Collections.singletonList;

@Log4j2
@Controller
@RequestMapping("/usuarios")
public class UsuarioController extends BaseController {

    @Autowired
    @Qualifier("usuarioService1")
    UsuarioService usuarioService;

    @Autowired
    @Qualifier("regionService1")
    RegionService regionService;

    @Autowired
    @Qualifier("comunaService1")
    ComunaService comunaService;

    @Autowired
    @Qualifier("estadoService1")
    EstadoService estadoService;

    @GetMapping
    public Object vistaBandejaUsuarios(ModelAndView mv) {
        try {
            mv.addObject("listadoUsuarios", usuarioService.bandeja());

        } catch (Exception e) {
            mv.addObject("message", singletonList(AppConstant.TRANSACCION_FALLIDO));
            mv.addObject("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
            mv.addObject("listadoUsuarios", new ArrayList<>());
            log.error(e);
        }

        mv.setViewName("mantenedor/usuarios/listar-usuarios");
        return mv;
    }

    @GetMapping("/form/{id}")
    public Object vistaVerUsuario(ModelAndView mv, @PathVariable("id") Integer id, RedirectAttributes redirAttrs) {
        try {
            UsuarioDTO usuario = usuarioService.buscarPorId(id);
            if (usuario == null) {
                redirAttrs.addFlashAttribute("message", AppConstant.REGISTRO_NO_EXISTENTE);
                redirAttrs.addFlashAttribute("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
                return "redirect:/usuarios";
            }

            // carga de comuna y regiones segun el caso
            ComunaDTO comunaAsociada = usuario.getDireccion().getComuna();
            List<ComunaDTO> comunas = comunaService.listarPorRegion(comunaAsociada.getRegion());

            mv.addObject("estados", estadoService.listar());
            mv.addObject("regiones", regionService.listar());
            mv.addObject("comunas", comunas);
            mv.addObject("usuario", usuario);
            mv.setViewName("mantenedor/usuarios/registrar-usuarios");
            return mv;

        } catch (Exception e) {
            log.error(e);
            mv.addObject("message", singletonList(AppConstant.TRANSACCION_FALLIDO));
            redirAttrs.addFlashAttribute("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
            return "redirect:/usuarios";

        }

    }

    @GetMapping("/form")
    public Object vistaVerUsuario(ModelAndView mv) {
        UsuarioDTO usuario = new UsuarioDTO();

        mv.addObject("estados", estadoService.listar());
        mv.addObject("regiones", regionService.listar());
        mv.addObject("comunas", new ArrayList<>());

        mv.addObject("usuario", usuario);
        mv.addObject("id", 0);
        mv.setViewName("mantenedor/usuarios/registrar-usuarios");
        return mv;
    }

    @ResponseBody
    @PostMapping("/form/{id}/eliminar")
    public Map<String, Object> eliminar(@PathVariable("id") Integer id, @RequestBody UsuarioDTO usuario) {
        Map<String, Object> res = new HashMap<>();
        Integer idJson = extraerId(usuario.getId());

        try {
            // UsuarioDTO usuarioEnSession = extraerUsuario(session);
            if (id == 0 | idJson == 0) {
                res.put("success", false);
                res.put("msg", "Error al eliminar usuario, falta el identificador");

            } else {
                // eliminar registro
                boolean esTransaccionOK = usuarioService.eliminar(idJson);

                registrarLog(ACCION_ELIMINACION, TABLA_REGION, idJson, usuario.toString(), esTransaccionOK);
                res.put("success", esTransaccionOK);
                res.put("msg", esTransaccionOK ? "OK" : "Error al eliminar usuario, falta el identificador");
            }
            return res;

        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }
    }


    @ResponseBody
    @PostMapping
    public Map<String, Object> guardar(@Valid @RequestBody UsuarioDTO usuario, BindingResult result) {
        Map<String, Object> res = new HashMap<>();

        try {
            // etapa de validacion
            List<String> listadoErroresValidacion = validarUsuario(result, usuario);

            // llenado de datos
            if (listadoErroresValidacion.size() > 0) {
                return respuestaError("Error de validacion", listadoErroresValidacion);
            }

            boolean success = (usuario.getId() == null) ? usuarioService.registrar(usuario) > 0 : usuarioService.actualizar(usuario);


            registrarLog((usuario.getId() == null) ? ACCION_CREACION : ACCION_MODIFICACION, TABLA_USUARIO, usuario.getId(), usuario.toString(), success);

            // etapa de guardado
            res.put("success", success);
            res.put("msg", success ? "Guardado exitosamente" : "Ha ocurrido un error al guardar");


            return res;
        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }

    }

    private List<String> validarUsuario(BindingResult result, UsuarioDTO usuario) {
        List<String> listadoErroresValidacion = new ArrayList<>();

        if (result.hasErrors()) {
            listadoErroresValidacion = AppConstant.validationErrors(result);
            ComunaDTO comuna = usuario.getDireccion().getComuna();
            if (comuna == null || comuna.getId() == null) {
                listadoErroresValidacion.add("* Debe especificar la comuna");

            }
        } else {
            if (usuario.getId() == null) {
                if (usuarioService.validarExistenciaPorCorreo(usuario.getCorreo())) {
                    listadoErroresValidacion.add("Ya existe un registro con el mail proporcionado");

                } else if (usuarioService.validarExistenciaPorRut(usuario.getRut())) {
                    listadoErroresValidacion.add("Ya existe un registro con el RUT proporcionado");

                }

            } else {
                // IDEA de mejora, modificar los cambios afectados como unicos
                // luego por la excepcion de claves detectarlos aca y enviarles un mensaje al usuario
                if (usuarioService.validarExistenciaNoIdPorCorreo(usuario.getId(), usuario.getCorreo())) {
                    listadoErroresValidacion.add("Correo ya ocupado en otra persona");

                } else if (usuarioService.validarExistenciaNoIdPorRut(usuario.getId(), usuario.getRut())) {
                    listadoErroresValidacion.add("Rut ya ocupado en otra persona");
                }
            }
        }

        return listadoErroresValidacion;
    }

}
