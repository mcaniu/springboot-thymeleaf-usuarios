package com.services;

import com.dto.RegionDTO;
import com.exceptions.UsuarioException;

import java.util.List;

public interface RegionService {

    Integer registrar(RegionDTO regionDTO);

    boolean eliminar(Integer id) throws UsuarioException;

    boolean actualizar(RegionDTO regionDTO) throws UsuarioException;

    RegionDTO buscarPorId(Integer id);

    List<RegionDTO> listar();

}
