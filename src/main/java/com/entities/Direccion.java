package com.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "DIRECCION")
@Getter
@Setter
public class Direccion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "DESCRIPCION")
    public String descripcion;

    @OneToOne
    @JoinColumn(name = "ID_COMUNA", referencedColumnName = "ID")
    public Comuna comuna;

}
