package com.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "LOG")
@Getter
@Setter
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "FECHA_MODIFICACION")
    private Date fechaModificacion;

    @ManyToOne
    @JoinColumn(name = "ID_USUARIO_MODIFICACION")
    private Usuario usuarioModificacion;

    @ManyToOne
    @JoinColumn(name = "ID_TIPO_ACCION", referencedColumnName = "ID", nullable=false)
    private TipoAccion tipoAccion;

    @Column(name = "NOMBRE_TABLA_AFECTADA")
    private String nombreTablaAfectada;

    @Column(name = "REQUEST")
    private String request;

    @Column(name = "ID_TABLA_AFECTADA")
    private Integer idtablaAfectada;

    @Column(name = "ES_EXITOSA")
    private Boolean esExitosa;

}
