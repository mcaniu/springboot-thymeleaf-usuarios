package com.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class LoginDTO {

    @NotBlank(message = "* El usuario no puede ir en blanco")
    private String usuario;

    @NotBlank(message = "* La contraseña no puede ir en blanco")
    private String contrasena;

}
