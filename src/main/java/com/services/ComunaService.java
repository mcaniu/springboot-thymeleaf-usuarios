package com.services;

import com.dto.ComunaDTO;
import com.dto.RegionDTO;
import com.exceptions.UsuarioException;

import java.util.List;

public interface ComunaService {

    Integer registrar(ComunaDTO comunaDTO);

    boolean eliminar(Integer id) throws UsuarioException;

    boolean actualizar(ComunaDTO comunaDTO) throws UsuarioException;

    ComunaDTO buscarPorId(Integer id);

    List<ComunaDTO> listar();

    List<ComunaDTO> listarPorRegion(RegionDTO regionDTO);

}
