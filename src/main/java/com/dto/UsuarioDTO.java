package com.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

@ToString
@Getter
@Setter
public class UsuarioDTO {

    public Integer id;

    // @Pattern(regexp = "^\\d{1,2}\\.\\d{3}\\.\\d{3}[-][0-9kK]{1}$", message = "* El RUT es invalido")
    @NotBlank(message = "* El RUT no puede ir vacio")
    @NotNull(message = "* El RUT no puede ir vacio")
    public String rut;

    @NotNull(message = "* El nombre no puede ir vacio")
    @NotBlank(message = "* El nombre no puede ir vacio")
    public String nombres;

    @NotNull(message = "* El apellido paterno no puede ir vacio")
    @NotBlank(message = "* El apellido paterno no puede ir vacio")
    public String apellidoP;

    @NotNull(message = "* El apellido materno no puede ir vacio")
    @NotBlank(message = "* El apellido materno no puede ir vacio")
    public String apellidoM;

    @NotNull(message = "* La fecha de nacimiento no puede ir vacio")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Date fechaNacimiento;

    @Email(message = "Debe ingresar un correo valido")
    @NotBlank(message = "* El correo no puede ir vacio")
    @NotNull(message = "* El correo no puede ir vacio")
    public String correo;

    public String pass;
    public EstadoDTO estado;

    @Valid
    public DireccionDTO direccion;

    public Boolean activo = true;

}
