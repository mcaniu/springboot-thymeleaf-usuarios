package com.services;

import com.dto.EstadoDTO;
import com.exceptions.UsuarioException;

import java.util.List;

public interface EstadoService {

    Integer registrar(EstadoDTO usuarioDTO);

    boolean eliminar(Integer id) throws UsuarioException;

    boolean actualizar(EstadoDTO usuarioDTO) throws UsuarioException;

    EstadoDTO buscarPorId(Integer id);

    List<EstadoDTO> listar();

}
