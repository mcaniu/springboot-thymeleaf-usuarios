package com.services;

import org.springframework.security.core.Authentication;

public interface AutentificacionService {

    Authentication getAuthentication();

}
