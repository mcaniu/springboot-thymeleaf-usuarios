package com.services.impl;

import com.dto.EstadoDTO;
import com.dto.MailDTO;
import com.dto.UsuarioDTO;
import com.entities.Comuna;
import com.entities.Direccion;
import com.entities.Estado;
import com.entities.Usuario;
import com.exceptions.UsuarioException;
import com.mappers.DireccionMapper;
import com.mappers.UsuarioMapper;
import com.repository.*;
import com.services.MailService;
import com.services.UsuarioService;
import com.utils.AppConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.utils.AppConstant.*;

@Service("usuarioService1")
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    EstadoRepository estadoRepository;

    @Autowired
    DireccionRepository direccionRepository;

    @Autowired
    ComunaRepository comunaRepository;

    @Autowired
    PerfilRepository perfilRepository;

    @Autowired
    private MailService mailService;

    @PersistenceContext(unitName = "h2")
    @Qualifier(value = "h2EntityManagerFactory")
    private EntityManager em;

    @Override
    public boolean validarExistenciaNoIdPorCorreo(Integer id, String correo) {
        return usuarioRepository.existsByIdNotAndCorreo(id, correo);
    }

    @Override
    public boolean validarExistenciaNoIdPorRut(Integer id, String rut) {
        return usuarioRepository.existsByIdNotAndRut(id, rut);
    }

    @Override
    public boolean validarExistenciaPorCorreo(String correo) {
        return usuarioRepository.existsByCorreo(correo);
    }

    @Override
    public boolean validarExistenciaPorRut(String rut) {
        return usuarioRepository.existsByRut(rut);
    }

    @Transactional
    @Override
    public Integer registrar(UsuarioDTO usuarioDTO) throws UsuarioException {
        Usuario usuario = UsuarioMapper.INSTANCE.dtoToObject(usuarioDTO);
        // estado.setFechaCreacion(new Date());
        // estado.setUsuarioCreacion(usuarioDTO.getUsername());

        try {
            String gPass = AppConstant.generarContrasenaAleatoria();
            Estado estado = estadoRepository.findById(SIN_CONFIRMACION).orElse(null);
            usuario.setEstado(estado);

            Direccion direccion = DireccionMapper.INSTANCE.dtoToObject(usuarioDTO.getDireccion());
            direccionRepository.save(direccion);

            usuario.setDireccion(direccion);
            usuario.setActivo(true);
            usuario.setPass(passwordEncoder.encode(gPass));

            usuario.setPerfil(perfilRepository.findById(ROLE_USUARIO).orElse(null));
            usuarioRepository.save(usuario);

            if (usuario.getId() != null && usuario.getId() > 0) {
                // ENVIO DE CORREOS
                MailDTO mailDTO = MailDTO.builder().to("mcaniu.apiux@supereduc.cl").subject(MAIL_ASUNTO1).build();

                Map<String, Object> camposDinamicos = new HashMap<>();
                camposDinamicos.put("user", usuario.getCorreo());
                camposDinamicos.put("pass", gPass);

                try {
                    mailService.sendMessageUsingThymeleafTemplateWithAttachment(mailDTO, camposDinamicos, MAIL_PLANTILLA1);

                } catch (MessagingException e) {
                    e.printStackTrace();
                }
            }
            return usuario.getId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Transactional
    @Override
    public boolean eliminar(Integer id) throws UsuarioException {
        Usuario usuario = usuarioRepository.findById(id).orElse(null);
        generarExceptionObjetoNulo(usuario);

        usuario.setActivo(false);
        usuarioRepository.save(usuario);
        return true;
    }

    @Transactional
    @Override
    public boolean actualizar(UsuarioDTO usuarioDTO) throws UsuarioException {
        Usuario usuarioOld = usuarioRepository.findById(usuarioDTO.getId()).orElse(null);

        generarExceptionObjetoNulo(usuarioOld);
        String correoOld = usuarioOld.getCorreo() == null ? "" : usuarioOld.getCorreo();

        usuarioOld.setRut(usuarioDTO.getRut());
        usuarioOld.setNombres(usuarioDTO.getNombres());
        usuarioOld.setApellidoM(usuarioDTO.getApellidoM());
        usuarioOld.setApellidoP(usuarioDTO.getApellidoP());
        usuarioOld.setFechaNacimiento(usuarioDTO.getFechaNacimiento());
        usuarioOld.setPass(passwordEncoder.encode(usuarioDTO.getPass()));
        usuarioOld.setCorreo(usuarioDTO.getCorreo());

        if ((usuarioDTO.getEstado().getId() != null && usuarioDTO.getEstado().getId() != 0)) {
            Estado estado = estadoRepository.findById(usuarioDTO.getEstado().getId()).orElse(null);
            usuarioOld.setEstado(estado);
        }

        usuarioOld.setEstado((!correoOld.equals(usuarioDTO.getCorreo())) ?
                estadoRepository.findById(SIN_CONFIRMACION).orElse(null) : usuarioOld.getEstado());


        // se actualiza o crea la direccion
        Direccion direccion = (usuarioOld.getDireccion() == null) ? new Direccion() : usuarioOld.getDireccion();
        Comuna comuna = comunaRepository.findById(usuarioDTO.getDireccion().getComuna().getId()).orElse(null);

        String nombreDireccion = usuarioDTO.getDireccion().getDescripcion();

        direccion.setComuna(comuna);
        direccion.setDescripcion(nombreDireccion == null ? "" : nombreDireccion);
        direccion = direccionRepository.save(direccion);

        usuarioOld.setDireccion(direccion);

        usuarioRepository.save(usuarioOld);


        if (!correoOld.equals(usuarioDTO.getCorreo())) {
            // ENVIO DE CORREOS
            MailDTO mailDTO = MailDTO.builder().to("mcaniu.apiux@supereduc.cl").subject(MAIL_ASUNTO2).build();

            Map<String, Object> camposDinamicos = new HashMap<>();
            camposDinamicos.put("user", usuarioOld.getCorreo());
            camposDinamicos.put("pass", usuarioDTO.getPass());

            try {
                mailService.sendMessageUsingThymeleafTemplateWithAttachment(mailDTO, camposDinamicos, MAIL_PLANTILLA1);

            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public UsuarioDTO buscarPorId(Integer id) {
        Usuario usuario = usuarioRepository.findByIdAndActivoTrue(id).orElse(null);
        return (id != null && id != 0) ?
                UsuarioMapper.INSTANCE.objectToDto(usuario) :
                null;
    }

    @Override
    public List<UsuarioDTO> listar() {
        List<Usuario> usuarios = usuarioRepository.findAllByActivoTrue();
        return UsuarioMapper.INSTANCE.map(usuarios);
    }

    @Override
    public List<UsuarioDTO> bandeja() {
        StringBuilder sQuery = new StringBuilder("SELECT u.id, u.nombres, u.correo, u.id_estado, e.descripcion ");
        sQuery.append("FROM usuario u JOIN estado e ON e.id = u.id_estado where u.activo=1");
        Query q = em.createNativeQuery(sQuery.toString());
        List<Object[]> result = q.getResultList();
        List<UsuarioDTO> usuarios = new ArrayList<>();

        UsuarioDTO plantillaUsuario;
        EstadoDTO plantillaEstado;
        for (Object[] u : result) {
            plantillaUsuario = new UsuarioDTO();
            plantillaUsuario.setId(Integer.parseInt(u[0].toString()));
            plantillaUsuario.setNombres(u[1].toString());
            plantillaUsuario.setCorreo(u[2].toString());

            plantillaEstado = new EstadoDTO();
            plantillaEstado.setId(Integer.parseInt(u[3].toString()));
            plantillaEstado.setDescripcion(u[4].toString());
            plantillaUsuario.setEstado(plantillaEstado);

            usuarios.add(plantillaUsuario);

        }

        return usuarios;
    }
}
