package com.controllers;

import com.dto.EstadoDTO;
import com.services.EstadoService;
import com.utils.AppConstant;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.utils.AppConstant.*;
import static java.util.Collections.singletonList;

@Log4j2
@Controller
@RequestMapping("/estados")
public class EstadoController extends BaseController {

    @Autowired
    @Qualifier("estadoService1")
    EstadoService estadoService;

    @Secured("ROLE_ADMINISTRADOR")
    @GetMapping()
    public Object vistaBandejaEstados(ModelAndView mv) {
        try {

            mv.addObject("listadoEstados", estadoService.listar());

        } catch (Exception e) {
            mv.addObject("message", singletonList(AppConstant.TRANSACCION_FALLIDO));
            mv.addObject("alertType", TRANSACCION_FALLIDO_ALERT);
            mv.addObject("listadoestados", new ArrayList<>());
            log.error(e);
        }

        mv.setViewName("mantenedor/estados/listar-estados");
        return mv;
    }

    @GetMapping("/form/{id}")
    public Object vistaVerEstado(ModelAndView mv, @PathVariable("id") Integer id, RedirectAttributes redirAttrs) {
        try {
            EstadoDTO estado = estadoService.buscarPorId(id);
            if (estado == null) {
                redirAttrs.addFlashAttribute("message", AppConstant.REGISTRO_NO_EXISTENTE);
                redirAttrs.addFlashAttribute("alertType", TRANSACCION_FALLIDO_ALERT);
                return "redirect:/estados";
            }

            mv.addObject("estado", estado);
            mv.setViewName("mantenedor/estados/registrar-estados");
            return mv;

        } catch (Exception e) {
            log.error(e);
            mv.addObject("message", singletonList(AppConstant.TRANSACCION_FALLIDO));
            redirAttrs.addFlashAttribute("alertType", TRANSACCION_FALLIDO_ALERT);
            return "redirect:/estados";

        }

    }

    @GetMapping("/form")
    public Object vistaVerEstado(ModelAndView mv) {
        EstadoDTO estado = new EstadoDTO();
        estado.setActivo(true);

        mv.addObject("estado", estado);
        mv.addObject("id", 0);
        mv.setViewName("mantenedor/estados/registrar-estados");
        return mv;
    }


    //metodo para insertar o actualizar datos
    @ResponseBody
    @PostMapping
    public Map<String, Object> guardar(@Valid @RequestBody EstadoDTO estado, BindingResult result) {
        log.info("guardando....");
        Map<String, Object> res = new HashMap<>();

        try {
            // UsuarioDTO usuarioEnSession = extraerUsuario(session);
            if (result.hasErrors()) {
                List<String> listadoErroresValidacion = AppConstant.validationErrors(result);
                return respuestaError("Error de validacion", listadoErroresValidacion);
            }

            boolean success = (estado.getId() == null) ?
                    estadoService.registrar(estado) > 0 :
                    estadoService.actualizar(estado);

            registrarLog((estado.getId() == null) ? ACCION_CREACION : ACCION_MODIFICACION, TABLA_ESTADO, estado.getId(), estado.toString(), success);


            // etapa de guardado
            res.put("success", success);
            res.put("msg", success ? "Guardado exitosamente" : "Ha ocurrido un error al guardar");

            return res;

        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }

    }

    @ResponseBody
    @PostMapping("/form/{id}/eliminar")
    public Map<String, Object> eliminar(@PathVariable("id") Integer id, @RequestBody EstadoDTO estado) {
        Map<String, Object> res = new HashMap<>();
        Integer idJson = extraerId(estado.getId());
        try {
            // UsuarioDTO usuarioEnSession = extraerUsuario(session);
            if (id == 0 | idJson == 0) {
                res.put("success", false);
                res.put("msg", "Error al eliminar estado, falta el identificador");

            } else {
                // eliminar registro
                boolean esTransaccionOK = estadoService.eliminar(idJson);

                registrarLog(ACCION_ELIMINACION, TABLA_ESTADO, idJson, estado.toString(), esTransaccionOK);

                res.put("success", esTransaccionOK);
                res.put("msg", esTransaccionOK ? "OK" : "Error al eliminar estado, falta el identificador");
            }
            return res;

        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }

    }


}
