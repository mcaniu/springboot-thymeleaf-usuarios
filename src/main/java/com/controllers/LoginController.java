package com.controllers;

import com.dto.ComunaDTO;
import com.dto.CustomUserDetailsDTO;
import com.dto.UsuarioDTO;
import com.services.*;
import com.utils.AppConstant;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

import static java.util.Collections.singletonList;

@Log4j2
@Controller
public class LoginController {

    @Autowired
    private AutentificacionService autentificacionService;

    @Autowired
    @Qualifier("usuarioService1")
    UsuarioService usuarioService;

    @Autowired
    @Qualifier("comunaService1")
    ComunaService comunaService;

    @Autowired
    @Qualifier("estadoService1")
    EstadoService estadoService;

    @Autowired
    @Qualifier("regionService1")
    RegionService regionService;

    @GetMapping("/login")
    public Object login(ModelAndView mv, String error) {
        if (error != null) {
            mv.addObject("message", "Usuario o password incorrecto");
            mv.addObject("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
        }

        mv.setViewName("login/login");
        return mv;
    }

    @GetMapping("/mi-perfil")
    public Object miPerfilVer(ModelAndView mv, RedirectAttributes redirAttrs) {
        try {
            Authentication authentication = autentificacionService.getAuthentication();

            if (authentication.getPrincipal() instanceof CustomUserDetailsDTO) {
                Integer id = ((CustomUserDetailsDTO) authentication.getPrincipal()).getId();
                if (id == null || id == 0) {
                    return "redirect:/login";
                }
                UsuarioDTO usuario = usuarioService.buscarPorId(id);

                if (usuario == null) {
                    return "redirect:/login";
                }

                // carga de comuna y regiones segun el caso
                ComunaDTO comunaAsociada = usuario.getDireccion().getComuna();
                List<ComunaDTO> comunas = comunaService.listarPorRegion(comunaAsociada.getRegion());

                mv.addObject("estados", singletonList(usuario.getEstado()));
                mv.addObject("regiones", regionService.listar());
                mv.addObject("comunas", comunas);
                mv.addObject("usuario", usuario);
                mv.setViewName("mi-perfil/mi-perfil");
                return mv;

            } else {
                return "redirect:/login";
            }


        } catch (Exception e) {
            log.error(e);
            mv.addObject("message", singletonList(AppConstant.TRANSACCION_FALLIDO));
            redirAttrs.addFlashAttribute("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
            return "redirect:/login";

        }

    }

}
