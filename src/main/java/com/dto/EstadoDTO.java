package com.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@ToString
@Getter
@Setter
public class EstadoDTO implements Serializable {

    private Integer id;

    @NotNull(message = "* El nombre estado no puede ir vacio")
    @NotBlank(message = "* El nombre estado no puede ir vacio")
    private String descripcion;
    private Boolean activo;

}
