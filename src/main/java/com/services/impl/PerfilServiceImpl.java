package com.services.impl;

import com.dto.PerfilDTO;
import com.entities.Perfil;
import com.exceptions.UsuarioException;
import com.mappers.PerfilMapper;
import com.repository.PerfilRepository;
import com.services.PerfilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.utils.AppConstant.generarExceptionObjetoNulo;

@Service("perfilService1")
public class PerfilServiceImpl implements PerfilService {

    @Autowired
    PerfilRepository perfilRepository;

    @Transactional
    @Override
    public Integer registrar(PerfilDTO perfilDTO) {
        Perfil perfil = PerfilMapper.INSTANCE.dtoToObject(perfilDTO);
        // estado.setFechaCreacion(new Date());
        // estado.setUsuarioCreacion(usuarioDTO.getUsername());

        perfilRepository.save(perfil);
        return perfil.getId();
    }

    @Override
    public boolean eliminar(Integer id) throws UsuarioException {
        Perfil perfil = perfilRepository.findById(id).orElse(null);
        generarExceptionObjetoNulo(perfil);

        perfilRepository.delete(perfil);
        return true;
    }

    @Transactional
    @Override
    public boolean actualizar(PerfilDTO perfilDTO) throws UsuarioException {
        Perfil perfilOld = perfilRepository.findById(perfilDTO.getId()).orElse(null);
        generarExceptionObjetoNulo(perfilOld);

        perfilOld.setNombre(perfilDTO.getNombre());
        perfilOld.setActivo(perfilDTO.getActivo());
        perfilRepository.save(perfilOld);

        return true;
    }

    @Override
    public PerfilDTO buscarPorId(Integer id) {
        return (id != null && id != 0) ?
                PerfilMapper.INSTANCE.objectToDto(perfilRepository.findById(id).orElse(null)) :
                null;
    }

    @Override
    public List<PerfilDTO> listar() {
        List<Perfil> perfils = perfilRepository.findAll();
        return PerfilMapper.INSTANCE.map(perfils);
    }
}
