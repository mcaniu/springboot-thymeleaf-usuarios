package com.controllers;

import com.dto.ComunaDTO;
import com.dto.RegionDTO;
import com.services.ComunaService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

import static com.utils.AppConstant.*;


@Log4j2
@Controller
@RequestMapping("/comunas")
public class ComunaController extends BaseController {

    @Autowired
    @Qualifier("comunaService1")
    ComunaService comunaService;

    @ResponseBody
    @PostMapping("/form/{id}/eliminar")
    public Map<String, Object> eliminar(@PathVariable("id") Integer id, @Valid @RequestBody ComunaDTO comuna) {
        Map<String, Object> res = new HashMap<>();
        Integer idJson = extraerId(comuna.getId());
        try {
            if (id == 0 | idJson == 0) {
                log.error("Error al eliminar comuna, falta el identificador");

                res.put("success", false);
                res.put("msg", "Error al eliminar comuna, falta el identificador");

            } else {
                // eliminar registro
                boolean esTransaccionOK = comunaService.eliminar(idJson);

                registrarLog(ACCION_ELIMINACION, TABLA_COMUNA, idJson, comuna.toString(), esTransaccionOK);


                res.put("success", esTransaccionOK);
                res.put("msg", esTransaccionOK ? "OK" : "Error al eliminar comuna, falta el identificador");
                log.info(esTransaccionOK ? "OK" : "Error al eliminar comuna, falta el identificador");
            }
            return res;

        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }

    }

    @ResponseBody
    @PostMapping
    public Map<String, Object> guardar(@RequestBody ComunaDTO comuna) {
        Map<String, Object> res = new HashMap<>();

        try {
            if (comuna.getNombre() == null || comuna.getNombre().isEmpty()) {
                res.put("msg", "Error al guardar la comuna");

            } else {
                boolean esTransaccionOK;
                if (comuna.getId() == null) {
                    RegionDTO regionDTO = new RegionDTO();
                    regionDTO.setId(comuna.getRegion().getId());

                    comuna.setRegion(regionDTO);

                    Integer idInsertado = comunaService.registrar(comuna);
                    esTransaccionOK = (idInsertado != null && idInsertado > 0);

                    registrarLog(ACCION_CREACION, TABLA_COMUNA, idInsertado, comuna.toString(), esTransaccionOK);

                } else {
                    esTransaccionOK = comunaService.actualizar(comuna);
                    registrarLog(ACCION_MODIFICACION, TABLA_COMUNA, comuna.getId(), comuna.toString(), esTransaccionOK);

                }

                res.put("success", esTransaccionOK);
                res.put("msg", esTransaccionOK ? "Guardado exitosamente" : "Ha ocurrido un error al guardar");

            }
            return res;

        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }

    }
}
