package com.mappers;

import com.dto.DireccionDTO;
import com.entities.Direccion;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DireccionMapper {
    DireccionMapper INSTANCE = Mappers.getMapper(DireccionMapper.class);

    @Mapping(source = "id", target = "id")
    DireccionDTO objectToDto(Direccion direccion);

    @Mapping(source = "id", target = "id")
    Direccion dtoToObject(DireccionDTO direccionDTO);

    List<DireccionDTO> map(List<Direccion> direcciones);

    List<Direccion> mapDtos(List<DireccionDTO> direccionDTOS);

}
