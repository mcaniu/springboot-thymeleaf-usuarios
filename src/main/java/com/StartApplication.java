package com;

import com.configuration.SecurityConfig;
import com.dto.MailDTO;
import com.services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.Map;

import static com.utils.AppConstant.MAIL_ASUNTO1;
import static com.utils.AppConstant.MAIL_PLANTILLA1;

@SpringBootApplication
@ComponentScan("com.configuration")
@ComponentScan("com.controllers")
@ComponentScan("com.services")
public class StartApplication implements WebApplicationInitializer, CommandLineRunner { //implements CommandLineRunner {

    @Autowired
    private MailService mailService;

    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class, args);
    }

    @Override
    public void run(String... arg) {
        // ENVIO DE PRUEBA DE CORREOS
        MailDTO mailDTO = MailDTO.builder().to("mcaniu.apiux@supereduc.cl").subject(MAIL_ASUNTO1).build();
        // details.setAttachment("c://directorio/archivo.algo");// correo adjunto no implementado
        Map<String, Object> camposDinamicos = new HashMap<>();
        camposDinamicos.put("user", "pepito");
        camposDinamicos.put("pass", "12234");

        try {
            mailService.sendMessageUsingThymeleafTemplateWithAttachment(mailDTO, camposDinamicos, MAIL_PLANTILLA1);
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        // CREACION DE DIRECTORIOS
        // FileUtil.eliminarTodo();
        // FileUtil.inicializarDirectorio();
    }

    @Override
    public void onStartup(ServletContext sc) {

        AnnotationConfigWebApplicationContext root = new AnnotationConfigWebApplicationContext();
        root.register(SecurityConfig.class);

        sc.addListener(new ContextLoaderListener(root));
        sc.addFilter("securityFilter", new DelegatingFilterProxy("springSecurityFilterChain"))
                .addMappingForUrlPatterns(null, false, "/*");
    }
}
