package com.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ComunaDTO {

    public Integer id;

    public String nombre;
    public Boolean activo = true;
    public RegionDTO region;
}
