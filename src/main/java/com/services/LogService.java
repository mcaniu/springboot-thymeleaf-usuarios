package com.services;

public interface LogService {

    void registrar(Integer idUsuarioModificacion,
                   Integer idTipoAccion,
                   String nombreTablaAfectada,
                   Integer idTablaAfectada,
                   String request,
                   boolean esTransaccionExitosa);

}
