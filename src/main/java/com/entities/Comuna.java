package com.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "COMUNA")
@Getter
@Setter
public class Comuna {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "NOMBRE")
    public String nombre;

    @ManyToOne
    @JoinColumn(name = "ID_REGION", referencedColumnName = "ID", nullable=false)
    public Region region;

    @Column(name = "ACTIVO")
    public Boolean activo = true;

}
