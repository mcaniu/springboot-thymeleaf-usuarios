package com.controllers;

import com.dto.PerfilDTO;
import com.services.PerfilService;
import com.utils.AppConstant;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.utils.AppConstant.*;
import static java.util.Collections.singletonList;

@Log4j2
@Controller
@RequestMapping("/perfiles")
public class PerfilController extends BaseController {

    @Autowired
    @Qualifier("perfilService1")
    PerfilService perfilService;

    @GetMapping()
    public Object vistaBandejaPerfiles(ModelAndView mv) {
        try {
            // UsuarioDTO usuario = (UsuarioDTO) session.getAttribute("usuario");
            mv.addObject("listadoPerfiles", perfilService.listar());

        } catch (Exception e) {
            mv.addObject("message", singletonList(AppConstant.TRANSACCION_FALLIDO));
            mv.addObject("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
            mv.addObject("listadoPerfiles", new ArrayList<>());
            log.error(e);
        }

        mv.setViewName("mantenedor/perfiles/listar-perfiles");
        return mv;
    }

    @GetMapping("/form/{id}")
    public Object vistaVerPerfil(ModelAndView mv, @PathVariable("id") Integer id, RedirectAttributes redirAttrs) {
        try {
            PerfilDTO perfil = perfilService.buscarPorId(id);
            if (perfil == null) {
                redirAttrs.addFlashAttribute("message", AppConstant.REGISTRO_NO_EXISTENTE);
                redirAttrs.addFlashAttribute("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
                return "redirect:/perfiles";
            }

            mv.addObject("perfil", perfil);
            mv.setViewName("mantenedor/perfiles/registrar-perfiles");
            return mv;

        } catch (Exception e) {
            log.error(e);
            mv.addObject("message", singletonList(AppConstant.TRANSACCION_FALLIDO));
            redirAttrs.addFlashAttribute("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
            return "redirect:/perfiles";

        }

    }

    @GetMapping("/form")
    public Object vistaVerPerfil(ModelAndView mv) {
        PerfilDTO perfil = new PerfilDTO();
        perfil.setActivo(true);

        mv.addObject("perfil", perfil);
        mv.addObject("id", 0);
        mv.setViewName("mantenedor/perfiles/registrar-perfiles");
        return mv;
    }


    //metodo para insertar o actualizar datos
    @ResponseBody
    @PostMapping
    public Map<String, Object> guardar(@Valid @RequestBody PerfilDTO perfil, BindingResult result) {
        log.info("guardando....");
        Map<String, Object> res = new HashMap<>();

        try {
            // UsuarioDTO usuarioEnSession = extraerUsuario(session);
            if (result.hasErrors()) {
                List<String> listadoErroresValidacion = AppConstant.validationErrors(result);
                return respuestaError("Error de validacion", listadoErroresValidacion);
            }

            boolean success = (perfil.getId() == null) ? perfilService.registrar(perfil) > 0 : perfilService.actualizar(perfil);

            registrarLog((perfil.getId() == null) ? ACCION_CREACION : ACCION_MODIFICACION, TABLA_PERFIL, perfil.getId(), perfil.toString(), success);

            // etapa de guardado
            res.put("success", success);
            res.put("msg", success ? "Guardado exitosamente" : "Ha ocurrido un error al guardar");


            return res;
        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }
    }


    @ResponseBody
    @PostMapping("/form/{id}/eliminar")
    public Map<String, Object> eliminar(@PathVariable("id") Integer id, @RequestBody PerfilDTO perfil) {
        Map<String, Object> res = new HashMap<>();
        Integer idJson = extraerId(perfil.getId());

        try {
            // UsuarioDTO usuarioEnSession = extraerUsuario(session);
            if (id == 0 | idJson == 0) {
                res.put("success", false);
                res.put("msg", "Error al eliminar perfil, falta el identificador");

            } else {
                // eliminar registro
                boolean esTransaccionOK = perfilService.eliminar(idJson);

                registrarLog(ACCION_ELIMINACION, TABLA_PERFIL, idJson, perfil.toString(), esTransaccionOK);

                res.put("success", esTransaccionOK);
                res.put("msg", esTransaccionOK ? "OK" : "Error al eliminar perfil, falta el identificador");
            }
            return res;

        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }
    }


}
