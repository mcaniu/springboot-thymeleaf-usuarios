package com.services.impl;

import com.dto.CustomUserDetailsDTO;
import com.entities.Usuario;
import com.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByCorreoAndActivoTrue(username).orElse(null);

        if (usuario == null) {
            throw new UsernameNotFoundException(username);
        }

        // System.out.println(passwordEncoder.encode(usuario.getPass()));
        return new CustomUserDetailsDTO(
                usuario.getCorreo(),
                usuario.getPass(),
                Collections.singletonList(new SimpleGrantedAuthority(usuario.getPerfil().getNombre())),
                usuario.getId(),
                usuario.getNombres(),
                usuario.getApellidoP(),
                usuario.getCorreo()
        );
    }

}
