package com.services;

import com.dto.UsuarioDTO;
import com.exceptions.UsuarioException;

import java.util.List;

public interface UsuarioService {

    Integer registrar(UsuarioDTO usuarioDTO) throws UsuarioException;

    boolean eliminar(Integer id) throws UsuarioException;

    boolean actualizar(UsuarioDTO usuarioDTO) throws UsuarioException;

    UsuarioDTO buscarPorId(Integer id);

    List<UsuarioDTO> listar();

    List<UsuarioDTO> bandeja();

    boolean validarExistenciaPorRut(String rut);

    boolean validarExistenciaPorCorreo(String correo);

    boolean validarExistenciaNoIdPorCorreo(Integer id, String correo);

    boolean validarExistenciaNoIdPorRut(Integer id, String rut);

}
