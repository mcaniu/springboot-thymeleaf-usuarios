package com.repository;

import com.entities.Comuna;
import com.entities.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComunaRepository extends JpaRepository<Comuna, Integer> {

    @Modifying
    @Query("DELETE FROM Comuna c WHERE  c.id=:id")
    void deleteComuna(@Param("id") Integer id);

    List<Comuna> findAllByRegion(Region region);

}
