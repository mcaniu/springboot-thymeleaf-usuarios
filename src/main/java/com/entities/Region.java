package com.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "REGION")
@Getter
@Setter
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "NOMBRE")
    public String nombre;

    @Column(name = "ACTIVO")
    public Boolean activo = true;

}
