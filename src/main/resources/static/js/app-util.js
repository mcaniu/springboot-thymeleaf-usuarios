const EMPTY_VALUE = "";
const GLOSA_INFO = "Información";
const GLOSA_ERROR1 = "Network response was not ok.";
const GLOSA_ERROR2 = "Hubo un problema con la solicitud:";
const GLOSA_ERROR3 = "Ha ocurrido un problema al guardar: ";
const GLOSA_DESCONOCIDO = "Desconocido";
const GLOSA_VALIDACION1 = "Complete los siguientes campos:";
const GLOSA_ELIMINACION = "Esta Seguro de eliminar el registro?";
const SA_SUCCESS = "success";
const SA_ERROR = "error";
const SA_INFO = "info";
const SA_WARNING = "warning";

$(document).ready(function () {
    // carga de datatable
    $('#example').DataTable({
        searching: false,
        language: {
            url: '//cdn.datatables.net/plug-ins/1.13.4/i18n/es-ES.json',
        }
    });

    // setea el maximo del input fecha
    // se carga solo si la pantalla donde esta el input es cargada
    var fechaNacimiento = document.getElementById("nacimiento");

    if(fechaNacimiento != null){
        fechaNacimiento.setAttribute("max", fechaNoPermitidaStr());
    }




    $('#rut').on('propertychange input', function(ev) {
        // extraer valor despues del guion si es que lo tiene
        let rut = ev.target.value.replace(/[^\dKk]/g, ''); // Eliminar puntos y dejar solo dígitos y K/k
        if (rut.length > 1) {
            rut = rut.substring(0, rut.length - 1) + '-' + rut.charAt(rut.length - 1);
        }
        rut = rut.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'); // Agregar puntos cada 3 dígitos


        // se agregan los puntos
        ev.target.value = rut;

    });

    var rutCargaInicial = $('#rut').val();


    if(rutCargaInicial!= undefined){
        let rut1 = rutCargaInicial.replace(/[^\dKk]/g, ''); // Eliminar puntos y dejar solo dígitos y K/k
        if (rut1.length > 1) {
            rut1 = rut1.substring(0, rut1.length - 1) + '-' + rut1.charAt(rut1.length - 1);
        }
        rut1 = rut1.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'); // Agregar puntos cada 3 dígitos

        // se agregan los puntos
        $('#rut').val(rut1);
    }


});

function modalSweetAlertGeneric(titulo = "", mensaje = "", nombreBoton, tipoAlerta = "warning"){
    swal({
        closeOnClickOutside: false, // no permite cerrar al clikcear afuera del modal
        closeOnEsc: false, // no permite cerrar con tecla esc
        title: titulo,
        text: mensaje,
        icon: tipoAlerta // "success", "error", "info", "warning"
    }).then((opcion) => {
        if(nombreBoton !== undefined){
            bloquearDesbloquearBoton(nombreBoton, true);
        }
    });
}

function modalSweetAlertSuccessGeneric(titulo = "", mensaje = "", esRecargaPaginaActual, urlRecarga = ""){

    swal({
        closeOnClickOutside: false, // no permite cerrar al clikcear afuera del modal
        closeOnEsc: false, // no permite cerrar con tecla esc
        title: titulo,
        text: mensaje,
        icon: SA_SUCCESS // "success", "error", "info", "warning"
    }).then((opcion) => {
        if(esRecargaPaginaActual){
            location.reload();
        }else {
            window.location.href = urlRecarga;
        }
    });
}

function modalEliminarComunaEnVistaDetalle(formRegionId, formComunaId) {
    // var nombreFormulario = "comuna" + formComunaId;
    // var form = document.getElementById(nombreFormulario);
    // form.action = "/website/regiones/form/" + formRegionId + "/comunas/eliminar";

    modalEliminarEnVistaDetalle('comunas', formComunaId, true);
}


function modalEliminarEnVistaDetalle(nombreVista, formId, esRecargaPaginaActual) {
    // var form = document.getElementById(formId);
    // form.action = "/website/" + nombreVista + "/form/" + formId + "/eliminar";
    esRecargaPaginaActual = esRecargaPaginaActual == undefined ? false : true;
    var url = "/website/" + nombreVista + "/form/" + formId + "/eliminar";
    var urlRecarga = "/website/" + nombreVista;

    var json = {
        "id": formId
    };

    swal({
        closeOnClickOutside: false, // no permite cerrar al clikcear afuera del modal
        closeOnEsc: false, // no permite cerrar con tecla esc
        title: EMPTY_VALUE,
        text: GLOSA_ELIMINACION,
        icon: SA_WARNING, // "success", "error", "info", "warning"
        dangerMode: true,
        buttons: {
            cancel: "Cancelar",
            confirm: {
                className: "",
                closeModal: false,
                text: "Eliminar",
                value: "eliminar",
            }
        }
    }).then((opcion) => {
        console.log(opcion);
        // setTimeout(document.getElementById(formId).submit(), 20000);
        switch (opcion) {
            case "eliminar":
                fetch(url, armarRequest(json))
                    .then(function(response) {
                        // Manejo de la respuesta del servidor
                        if (response.ok) {
                            // Si la respuesta es correcta, leemos el JSON de la respuesta
                            return response.json();
                        }else {
                            modalSweetAlertGeneric(GLOSA_INFO, GLOSA_ERROR1, null, SA_ERROR);
                        }
                    })
                    .then(function(data) {
                        // Hacemos algo con la respuesta JSON recibida
                        console.log(data);
                        if(data.validationErrors != undefined){
                            let errores = data.validationErrors;
                            let msgErroresValidacion = "";
                            for(let i=0; i < errores.length; i++) {
                                msgErroresValidacion = msgErroresValidacion + "\n " + errores[i];
                            }

                            modalSweetAlertGeneric(GLOSA_VALIDACION1, msgErroresValidacion, null, SA_INFO);
                            return 0;
                        }else {
                            if(!data.success){
                                let detalleError = data.msg == null? "Desconocido" : data.msg;

                                modalSweetAlertGeneric(GLOSA_INFO, "Ha ocurrido un problema al eliminar: " + detalleError, null, SA_ERROR);
                                return 0;
                            }

                            modalSweetAlertSuccessGeneric(GLOSA_INFO,  "Eliminado", esRecargaPaginaActual, urlRecarga);

                        }

                    })
                    .catch(function(error) {
                        // Capturamos y manejamos cualquier error que ocurra durante la solicitud
                        console.error(GLOSA_ERROR2, error);
                        modalSweetAlertGeneric(GLOSA_INFO, GLOSA_ERROR2 + error, null, SA_ERROR);
                        return 0;
                    });

                break;
            default:
            // swal("Good job!", "", "error");
        }
    });
}

function bloquearDesbloquearBoton(idBtn, esHabilitado) {
    var boton = document.getElementById(idBtn);
    if(boton == undefined){
        return;
    }

    var texto = boton.innerText;

    // Simulación de carga con un setTimeout (puedes reemplazarlo con tu lógica de carga real)
   // esHabilitado = (esHabilitado == undefined);
    if(esHabilitado){
        // Habilita el botón y restablece el texto original después de 3 segundos (simulado)
        // setTimeout(function() {
        boton.disabled = false;
        boton.innerHTML = '<i class="bi bi-pencil-square"></i>' + texto;

        // }, 3000);
    }else {
        // Deshabilita el botón al hacer clic
        boton.disabled = true;
        boton.innerHTML = '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span><i class="bi bi-pencil-square"></i>' + texto; // Agrega un spinner y texto de carga al botón

    }

}

function validarFormularioUsuario() {
    var existeErroresValidacion = false;
    var msgErrores = "";
    var formUsuario = document.fusuario;

    bloquearDesbloquearBoton('guardarUsuario', false);

    if (formUsuario.rut.value.length == EMPTY_VALUE) {
        // document.fvalida.nombre.focus();
        // document.fvalida.nacimiento.setAttribute("class", "form-control is-invalid");
        document.getElementById("nacimiento").setAttribute("class", "form-control is-invalid");
        msgErrores = msgErrores + "\n * El campo RUT no debe ir vacio";
        existeErroresValidacion = true;
    }else if(!validarRut(formUsuario.rut.value)){
        msgErrores = msgErrores + "\n * El formato del campo RUT es invalido, debe ser XXXXXXXX-X";
        existeErroresValidacion = true;
    }


    if (formUsuario.nacimiento.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo FECHA NACIMIENTO no debe ir vacia";
        existeErroresValidacion = true;
    }else if(validarFecha(formUsuario.nacimiento.value)){
        msgErrores = msgErrores + "\n * La de FECHA NACIMIENTO debe ser de una persona mayor de 18 años";
        existeErroresValidacion = true;
    }

    if (formUsuario.nombres.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo NOMBRE no debe ir vacio";
        existeErroresValidacion = true;
    }

    if (formUsuario.apellidop.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo APELLIDO PATERNO no debe ir vacio";
        existeErroresValidacion = true;
    }

    if (formUsuario.apellidom.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo APELLIDO MATERNO no debe ir vacio";
        existeErroresValidacion = true;
    }

    if (formUsuario.region.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * Debe seleccionar opción del campo REGIÓN";
        existeErroresValidacion = true;
    }else if(formUsuario.region.value == 0){
        msgErrores = msgErrores + "\n * Debe seleccionar opción del campo REGIÓN";
        existeErroresValidacion = true;
    }

    if (formUsuario.comuna.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * Debe seleccionar opción del campo COMUNA";
        existeErroresValidacion = true;
    }else if(formUsuario.comuna.value == 0){
        msgErrores = msgErrores + "\n * Debe seleccionar opción del campo COMUNA";
        existeErroresValidacion = true;
    }

    if (formUsuario.direccion.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo DIRECCIÓN no debe ir vacia";
        existeErroresValidacion = true;
    }

    if (formUsuario.pass != undefined && formUsuario.pass.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo CONTRASEÑA no debe ir vacia";
        existeErroresValidacion = true;
    }
    if (formUsuario.correo.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo CORREO no debe ir vacio";
        existeErroresValidacion = true;
    }else if(!validarEmail(formUsuario.correo.value)){
        msgErrores = msgErrores + "\n * El campo CORREO debe ser un correo valido";
        existeErroresValidacion = true;
    }


    var json = {
        "id": formUsuario.id.value,
        "rut": formUsuario.rut.value,
        "nombres": formUsuario.nombres.value,
        "apellidoP": formUsuario.apellidop.value,
        "apellidoM": formUsuario.apellidom.value,
        "fechaNacimiento": formUsuario.nacimiento.value,
        "correo": formUsuario.correo.value,
        "pass": (formUsuario.pass != undefined) ? formUsuario.pass.value : null,
        "direccion": {
            "descripcion": formUsuario.direccion.value,
            "comuna": {
                "id": formUsuario.comuna.value
            }
        },
        "estado": (formUsuario.estado == undefined) ? null: {"id": formUsuario.estado.value},

    };

    if (existeErroresValidacion) {
        modalSweetAlertGeneric(GLOSA_VALIDACION1, msgErrores, 'guardarUsuario', SA_INFO);
        return 0;
    }

    sendPost("/website/usuarios", json, 'guardarUsuario');
}

function validarFormularioEstado() {
    var existeErroresValidacion = false;
    var msgErrores = "";
    var formEstado = document.festado;

    bloquearDesbloquearBoton('guardarEstado', false);
    if (formEstado.descripcion.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo NOMBRE no debe ir vacio";
        existeErroresValidacion = true;
    }

    var json = {
        "id": formEstado.id.value,
        "activo": formEstado.activo.checked,
        "descripcion": (formEstado.descripcion == undefined) ? null: formEstado.descripcion.value,

    };

    if (existeErroresValidacion) {
        modalSweetAlertGeneric(GLOSA_VALIDACION1, msgErrores, 'guardarEstado', SA_INFO);
        return 0;
    }

    sendPost("/website/estados", json, 'guardarEstado');

}

function validarFormularioPerfil() {
    var existeErroresValidacion = false;
    var msgErrores = "";
    var formPerfil = document.fperfil;
    bloquearDesbloquearBoton('guardarPerfil', false);

    if (formPerfil.nombre.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo NOMBRE no debe ir vacio";
        existeErroresValidacion = true;
    }

    var json = {
        "id": formPerfil.id.value,
        "activo": formPerfil.activo.checked,
        "nombre": (formPerfil.nombre == undefined) ? null: formPerfil.nombre.value,

    };

    if (existeErroresValidacion) {
        modalSweetAlertGeneric(GLOSA_VALIDACION1, msgErrores, "guardarPerfil",SA_INFO);
        return 0;
    }

    sendPost("/website/perfiles", json, 'guardarPerfil');

}

function validarFormularioRegion() {
    var existeErroresValidacion = false;
    var msgErrores = "";
    var formRegion = document.fregion;
    bloquearDesbloquearBoton('guardarRegion', false);

    if (formRegion.nombre.value.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo NOMBRE no debe ir vacio";
        existeErroresValidacion = true;
    }

    var json = {
        "id": formRegion.id.value,
        "activo": formRegion.activo.checked,
        "nombre": (formRegion.nombre == undefined) ? null: formRegion.nombre.value,

    };

    if (existeErroresValidacion) {
        modalSweetAlertGeneric(GLOSA_VALIDACION1, msgErrores, 'guardarRegion',SA_INFO);
        return 0;
    }

    sendPost("/website/regiones", json, 'guardarRegion');

}

function validarFormularioComuna(formRegionId, formComunaId) {
    var existeErroresValidacion = false;
    var msgErrores = "";

    var esAdd = (typeof formComunaId === 'undefined' || formComunaId === null);
    var nombreFormulario = "comuna" + ((esAdd) ? "" : formComunaId);
    var formComuna = document.getElementById(nombreFormulario);
    // formComuna.action = "/website/regiones/" + formRegionId + "/comunas";

    var campo = ((esAdd) ? formComuna.nombrecomuna.value : formComuna.nombrecomunae.value);


    if (campo.length == EMPTY_VALUE) {
        msgErrores = msgErrores + "\n * El campo NOMBRE no debe ir vacio";
        existeErroresValidacion = true;
    }

    var json = {
        "id": (esAdd) ? null : formComunaId,
        "nombre": campo,
        "region": {
            "id": formRegionId
        },
    };

    if (existeErroresValidacion) {
        modalSweetAlertGeneric(GLOSA_VALIDACION1, msgErrores, null,SA_INFO);
        return 0;
    }

    sendPost("/website/comunas", json, null);

}

function enviarFormulario(form, existeErroresValidacion, msgErrores, idInputIdentificador) {
    if (existeErroresValidacion) {
        swal({
            title: GLOSA_VALIDACION1,
            text: msgErrores,
            icon: "info" // "success", "error", "info", "warning"
        });
        return 0;
    }

    form.submit();

}

function validarEmail(email){
    // esta cosa no funciona con dominios de correo propios
    var reg = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    var result = reg.test(email);

    if (result == false) {
        return false;
    }
    return true;
}

function validarFecha(fecha) {
    return (new Date(fecha) > new Date(fechaNoPermitidaStr()));
}

function fechaNoPermitidaStr() {
    var dateNow = new Date();
    var dd = dateNow.getDate();
    var mm = (dateNow.getMonth() + 1);

    if(dd < 10){
        dd = '0'+ dd
    }

    if(mm < 10){
        mm = '0'+ mm
    }
    return [(dateNow.getFullYear()-18), '-', mm , '-',dd].join('');
}

async function selectRegion() {
    var inputselectcomuna = document.getElementById('comuna');
    var opciones = inputselectcomuna.options;

    for(var i=0; i<opciones.length; i++) {
        if(opciones[i].innerHTML != 'Seleccione...') {
            inputselectcomuna.removeChild(opciones[i]);
            i--;
        }

    }
    var inputselectregion = document.getElementById("region");
    var regionId = inputselectregion.value;
    // var textoseleccionado = region.options[region.selectedIndex];

    if(regionId > 0){
        try {
            let response = await fetch("/website/regiones/"+ regionId +"/comunas");
            if (!response.ok) {
                console.error("ocurrio un error al consultar las comunas NOK");
                // inputselectcomuna.setAttribute("disabled", "disabled");
            }

            let comunas = await response.json();

            for(let i=0; i<comunas.length; i++) {
                let newOption = new Option(comunas[i].nombre, comunas[i].id);
                inputselectcomuna.add(newOption, undefined);
            }

        } catch (error) {
            console.error("ocurrio un error al consultar las comunas", error);

        }
    }

}

function validarRut(rutCompleto) {
    return (checkRut(rutCompleto));
}

function checkRut(rutCompleto) {
    // linea agregada para quitarles los puntos
    let rutSinPuntos = rutCompleto.replace(/[^\dKk]/g, ''); // Eliminar puntos y dejar solo dígitos y K/k
    if (rutSinPuntos.length > 1) {
        rutSinPuntos = rutSinPuntos.substring(0, rutSinPuntos.length - 1) + '-' + rutSinPuntos.charAt(rutSinPuntos.length - 1);
    }

    // validacion normal
    // validar rut sin digito verificador
    if (!/^[0-9]+-[0-9kK]{1}$/.test( rutSinPuntos ))
        return false;
    var tmp     = rutSinPuntos.split('-');
    var digv    = tmp[1];
    var rut     = tmp[0];
    if (digv == 'K') digv = 'k' ;
    return (checkDv(rut) == digv );

}

function checkDv(rut) {
    // validar digito verificador
    var M=0,S=1;
    for(;rut;rut=Math.floor(rut/10))
        S=(S+rut%10*(9-M++%6))%11;
    return S?S-1:'k';
}

function sendPost(url, json, nombreBoton) {
    fetch(url, armarRequest(json))
        .then(function(response) {
            bloquearDesbloquearBoton(nombreBoton, true);
            // Manejo de la respuesta del servidor
            if (response.ok) {
                // Si la respuesta es correcta, leemos el JSON de la respuesta
                return response.json();
            }else {
                modalSweetAlertGeneric(GLOSA_INFO, GLOSA_ERROR1, null, SA_INFO);
            }
        })
        .then(function(data) {
            // Hacemos algo con la respuesta JSON recibida
           console.log(data);
           if(data.validationErrors){
               let errores = data.validationErrors;
               let msgErroresValidacion = "";
               for(let i=0; i < errores.length; i++) {
                   msgErroresValidacion = msgErroresValidacion + "\n " + errores[i];
               }

               modalSweetAlertGeneric(GLOSA_VALIDACION1, msgErroresValidacion, null, SA_INFO);
               return 0;
           }else {
               if(!data.success){
                   let detalleError = data.msg == null? GLOSA_DESCONOCIDO : data.msg;

                   modalSweetAlertGeneric(GLOSA_INFO, GLOSA_ERROR3 + detalleError, null, SA_ERROR);
                   return 0;

               }
               modalSweetAlertSuccessGeneric(GLOSA_INFO,  "Guardado Exitosamente", true, "");

           }

        })
        .catch(function(error) {
            // Capturamos y manejamos cualquier error que ocurra durante la solicitud
            console.error(GLOSA_ERROR2, error);
            modalSweetAlertGeneric(GLOSA_INFO, GLOSA_ERROR2 + error, null, SA_ERROR);
            return 0;
        });

}

function armarRequest(json) {
    return {
        method: 'POST',
        headers: {'Content-Type': 'application/json',}, // Especificamos que estamos enviando JSON
        body: JSON.stringify(json) // Convertimos el objeto a formato JSON
    };
}

// function exportarPDF(tipoListado) {
//     if (tipoListado !== undefined || tipoListado != null || tipoListado || tipoListado !== '') {
//         window.open('/api/v1/' + tipoListado + '/reporte-pdf', '_blank');
//     }
//
// }
//
// function exportarExcel(tipoListado) {
//     if (tipoListado !== undefined || tipoListado != null || tipoListado || tipoListado !== '') {
//         window.open('/api/v1/' + tipoListado + '/reporte-excel', '_blank');
//     }
//
// }
//
// function descargarCsv(tipoListado) {
//     if (tipoListado !== undefined || tipoListado != null || tipoListado || tipoListado !== '') {
//         window.open('/api/v1/' + tipoListado + '/plantilla-csv', '_blank');
//     }
//
// }
//
// function fileSelectedChanged(obj) {
//     var filePath = obj.value;
//
//     var ext = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
//     if (ext !== 'csv') {
//         document.getElementById("archivo").value = '';
//     }
// }
//
// function subirArchivo() {
//     console.log('ejecutando...')
//     let photo = document.getElementById("archivo").files[0];
//     let formData = new FormData();
//
//     formData.append("archivo", photo);
//     fetch('/api/v1/archivos/cargar-csv', {method: "POST", body: formData});
// }


