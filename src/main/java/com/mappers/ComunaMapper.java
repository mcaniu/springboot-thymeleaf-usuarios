package com.mappers;

import com.dto.ComunaDTO;
import com.entities.Comuna;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ComunaMapper {
    ComunaMapper INSTANCE = Mappers.getMapper(ComunaMapper.class);

    @Mapping(source = "id", target = "id")
    ComunaDTO objectToDto(Comuna comuna);

    @Mapping(source = "id", target = "id")
    Comuna dtoToObject(ComunaDTO comunaDTO);

    List<ComunaDTO> map(List<Comuna> estados);

    List<Comuna> mapDtos(List<ComunaDTO> comunasDTOs);

}
