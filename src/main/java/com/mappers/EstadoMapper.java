package com.mappers;

import com.dto.EstadoDTO;
import com.entities.Estado;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface EstadoMapper {
    EstadoMapper INSTANCE = Mappers.getMapper(EstadoMapper.class);

    @Mapping(source = "id", target = "id")
    EstadoDTO objectToDto(Estado estado);

    @Mapping(source = "id", target = "id")
    Estado dtoToObject(EstadoDTO estadoDTO);

    List<EstadoDTO> map(List<Estado> estados);

    List<Estado> mapDtos(List<EstadoDTO> estadoDTOS);

}
