package com.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ToString
@Getter
@Setter
public class RegionDTO {

    public Integer id;

    @NotBlank(message = "* El nombre de región no puede ir vacia")
    @NotNull(message = "* El nombre de región no puede ir vacio")
    public String nombre;
    public Boolean activo = true;

}
