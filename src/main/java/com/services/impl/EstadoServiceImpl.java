package com.services.impl;

import com.dto.EstadoDTO;
import com.entities.Estado;
import com.exceptions.UsuarioException;
import com.mappers.EstadoMapper;
import com.repository.EstadoRepository;
import com.services.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.utils.AppConstant.generarExceptionObjetoNulo;

@Service("estadoService1")
public class EstadoServiceImpl implements EstadoService {

    @Autowired
    EstadoRepository estadoRepository;

    @Transactional
    @Override
    public Integer registrar(EstadoDTO estadoDTO) {
        Estado estado = EstadoMapper.INSTANCE.dtoToObject(estadoDTO);
        // estado.setFechaCreacion(new Date());
        // estado.setUsuarioCreacion(usuarioDTO.getUsername());

        estadoRepository.save(estado);
        return estado.getId();
    }

    @Override
    public boolean eliminar(Integer id) throws UsuarioException {
        Estado estadoeOld = estadoRepository.findById(id).orElse(null);
        generarExceptionObjetoNulo(estadoeOld);

        estadoeOld.setActivo(false);
        estadoRepository.delete(estadoeOld);
        return true;
    }

    @Transactional
    @Override
    public boolean actualizar(EstadoDTO estadoDTO) throws UsuarioException {
        Estado estadoOld = estadoRepository.findById(estadoDTO.getId()).orElse(null);
        generarExceptionObjetoNulo(estadoOld);

        estadoOld.setDescripcion(estadoDTO.getDescripcion());
        estadoOld.setActivo(estadoDTO.getActivo());
        estadoRepository.save(estadoOld);

        return true;
    }

    @Override
    public EstadoDTO buscarPorId(Integer id) {
        return (id != null && id != 0) ?
                EstadoMapper.INSTANCE.objectToDto(estadoRepository.findById(id).orElse(null)) :
                null;
    }

    @Override
    public List<EstadoDTO> listar() {
        List<Estado> estado = estadoRepository.findAll();
        return EstadoMapper.INSTANCE.map(estado);
    }
}
