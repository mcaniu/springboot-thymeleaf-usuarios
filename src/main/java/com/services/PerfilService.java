package com.services;

import com.dto.PerfilDTO;
import com.exceptions.UsuarioException;

import java.util.List;

public interface PerfilService {

    Integer registrar(PerfilDTO perfilDTO);

    boolean eliminar(Integer id) throws UsuarioException;

    boolean actualizar(PerfilDTO perfilDTO) throws UsuarioException;

    PerfilDTO buscarPorId(Integer id);

    List<PerfilDTO> listar();

}
