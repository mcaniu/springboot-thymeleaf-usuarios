package com.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@ToString
@Getter
@Setter
public class DireccionDTO {

    public Integer id;

    @NotBlank(message = "* La dirección no puede ir vacia")
    @NotNull(message = "* La dirección no puede ir vacio")
    public String descripcion;

    public ComunaDTO comuna;

}
