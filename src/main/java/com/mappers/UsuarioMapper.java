package com.mappers;

import com.dto.UsuarioDTO;
import com.entities.Estado;
import com.entities.Usuario;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UsuarioMapper {
    UsuarioMapper INSTANCE = Mappers.getMapper(UsuarioMapper.class);

    @Mapping(source = "id", target = "id")
    UsuarioDTO objectToDto(Usuario usuario);

    @Mapping(source = "id", target = "id")
    Usuario dtoToObject(UsuarioDTO usuarioDTO);

    List<UsuarioDTO> map(List<Usuario> usuarios);

    List<Usuario> mapDtos(List<UsuarioDTO> usuarioDTOS);

}
