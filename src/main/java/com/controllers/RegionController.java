package com.controllers;

import com.dto.ComunaDTO;
import com.dto.RegionDTO;
import com.services.ComunaService;
import com.services.RegionService;
import com.utils.AppConstant;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.utils.AppConstant.*;
import static java.util.Collections.singletonList;

@Log4j2
@Controller
@RequestMapping("/regiones")
public class RegionController extends BaseController {

    @Autowired
    @Qualifier("comunaService1")
    ComunaService comunaService;

    @Autowired
    @Qualifier("regionService1")
    RegionService regionService;


    @GetMapping()
    public Object vistaBandejaRegiones(ModelAndView mv) {
        try {
            // UsuarioDTO usuario = (UsuarioDTO) session.getAttribute("usuario");
            mv.addObject("listadoRegiones", regionService.listar());

        } catch (Exception e) {
            mv.addObject("message", singletonList(AppConstant.TRANSACCION_FALLIDO));
            mv.addObject("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
            mv.addObject("listadoRegiones", new ArrayList<>());
            log.error(e);
        }

        mv.setViewName("mantenedor/regiones/listar-regiones");
        return mv;
    }

    @GetMapping("/form/{id}")
    public Object vistaVerRegion(ModelAndView mv, @PathVariable("id") Integer id, RedirectAttributes redirAttrs) {
        try {
            RegionDTO region = regionService.buscarPorId(id);
            if (region == null) {
                redirAttrs.addFlashAttribute("message", AppConstant.REGISTRO_NO_EXISTENTE);
                redirAttrs.addFlashAttribute("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
                return "redirect:/regiones";
            }

            // cambiar a buscado por region
            List<ComunaDTO> listadoComunas = comunaService.listarPorRegion(region);

            mv.addObject("comuna", new ComunaDTO());
            mv.addObject("region", region);
            mv.addObject("listadoComunas", (listadoComunas == null) ? new ArrayList<>() : listadoComunas);
            mv.setViewName("mantenedor/regiones/registrar-regiones");
            return mv;

        } catch (Exception e) {
            log.error(e);
            mv.addObject("message", singletonList(AppConstant.TRANSACCION_FALLIDO));
            redirAttrs.addFlashAttribute("alertType", AppConstant.TRANSACCION_FALLIDO_ALERT);
            return "redirect:/regiones";

        }

    }

    @GetMapping("/form")
    public Object vistaVeRegion(ModelAndView mv) {
        RegionDTO region = new RegionDTO();
        region.setActivo(true);

        mv.addObject("region", region);
        mv.addObject("id", 0);
        mv.setViewName("mantenedor/regiones/registrar-regiones");
        return mv;
    }

    //metodo para insertar o actualizar datos
    @ResponseBody
    @PostMapping
    public Map<String, Object> guardar(@Valid @RequestBody RegionDTO region, BindingResult result) {
        log.info("guardando....");
        Map<String, Object> res = new HashMap<>();

        try {
            if (result.hasErrors()) {
                List<String> listadoErroresValidacion = AppConstant.validationErrors(result);
                return respuestaError("Error de validacion", listadoErroresValidacion);
            }

            boolean success = (region.getId() == null) ? regionService.registrar(region) > 0 : regionService.actualizar(region);

            registrarLog((region.getId() == null) ? ACCION_CREACION : ACCION_MODIFICACION, TABLA_REGION, region.getId(), region.toString(), success);

            // etapa de guardado
            res.put("success", success);
            res.put("msg", success ? "Guardado exitosamente" : "Ha ocurrido un error al guardar");

            return res;

        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }
    }


    //metodo para insertar o actualizar datos
    @ResponseBody
    @PostMapping("/form/{id}/eliminar")
    public Map<String, Object> eliminar(@PathVariable("id") Integer id, @RequestBody RegionDTO region) {
        Map<String, Object> res = new HashMap<>();
        Integer idJson = extraerId(region.getId());

        try {
            // UsuarioDTO usuarioEnSession = extraerUsuario(session);
            if (idJson == 0) {
                res.put("success", false);
                res.put("msg", "Error al eliminar región, falta el identificador");

            } else {
                // eliminar registro
                boolean esTransaccionOK = regionService.eliminar(idJson);
                registrarLog(ACCION_ELIMINACION, TABLA_REGION, idJson, region.toString(), esTransaccionOK);

                res.put("success", esTransaccionOK);
                res.put("msg", esTransaccionOK ? "OK" : "Error al eliminar región, falta el identificador");
            }
            return res;

        } catch (Exception e) {
            log.error(e);
            return respuestaError(e.getMessage(), null);
        }
    }


    @GetMapping("/{idRegion}/comunas")
    public @ResponseBody
    List<ComunaDTO> obtenerListadoComunasRaw(@PathVariable("idRegion") Integer idRegion) {
        RegionDTO regionDTO = new RegionDTO();
        regionDTO.setId(idRegion);

        return comunaService.listarPorRegion(regionDTO);
    }


}
