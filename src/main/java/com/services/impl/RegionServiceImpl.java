package com.services.impl;

import com.dto.RegionDTO;
import com.entities.Region;
import com.exceptions.UsuarioException;
import com.mappers.RegionMapper;
import com.repository.RegionRepository;
import com.services.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.utils.AppConstant.generarExceptionObjetoNulo;

@Service("regionService1")
public class RegionServiceImpl implements RegionService {

    @Autowired
    RegionRepository regionRepository;


    @Transactional
    @Override
    public Integer registrar(RegionDTO regionDTO) {
        Region region = RegionMapper.INSTANCE.dtoToObject(regionDTO);
        // estado.setFechaCreacion(new Date());
        // estado.setUsuarioCreacion(usuarioDTO.getUsername());

        regionRepository.save(region);
        return region.getId();
    }

    @Override
    public boolean eliminar(Integer id) throws UsuarioException {
        Region region = regionRepository.findById(id).orElse(null);
        generarExceptionObjetoNulo(region);

        regionRepository.delete(region);
        return true;
    }

    @Transactional
    @Override
    public boolean actualizar(RegionDTO regionDTO) throws UsuarioException {
        Region regionOld = regionRepository.findById(regionDTO.getId()).orElse(null);
        generarExceptionObjetoNulo(regionOld);

        regionOld.setNombre(regionDTO.getNombre());
        regionOld.setActivo(regionDTO.getActivo());
        regionRepository.save(regionOld);

        return true;
    }

    @Override
    public RegionDTO buscarPorId(Integer id) {
        return (id != null && id != 0) ?
                RegionMapper.INSTANCE.objectToDto(regionRepository.findById(id).orElse(null)) :
                null;
    }


    @Override
    public List<RegionDTO> listar() {
        List<Region> regiones = regionRepository.findAll();
        return RegionMapper.INSTANCE.map(regiones);
    }

}
