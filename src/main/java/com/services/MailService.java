package com.services;


import com.dto.MailDTO;

import javax.mail.MessagingException;
import java.util.Map;

public interface MailService {

    /**
     * Metodo que envia un correo usando una plantilla de thymeleaf con o sin archivos
     *
     * @param mailDTO to correo hacia donde va, subject asunto, templateModel cuerpo del mensaje, files archivos adjuntos.
     */
    void sendMessageUsingThymeleafTemplateWithAttachment(MailDTO mailDTO, Map<String, Object> camposDinamicos, String plantilla) throws MessagingException;

}
