package com.services.impl;

import com.entities.Log;
import com.entities.TipoAccion;
import com.entities.Usuario;
import com.repository.*;
import com.services.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service("logService1")
public class LogServiceImpl implements LogService {

    @Autowired
    LogRepository logRepository;

    @Autowired
    TipoAccionRepository tipoAccionRepository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Autowired
    PerfilRepository perfilRepository;

    @Autowired
    RegionRepository regionRepository;

    @Autowired
    ComunaRepository comunaRepository;

    @Autowired
    EstadoRepository estadoRepository;

    @Transactional
    @Override
    public void registrar(Integer idUsuarioModificacion,
                          Integer idTipoAccion,
                          String nombreTablaAfectada,
                          Integer idTablaAfectada,
                          String request,
                          boolean esTransaccionExitosa) {
        try {

            Usuario u = usuarioRepository.findById(idUsuarioModificacion).orElse(null);
            TipoAccion ta = tipoAccionRepository.findById(idTipoAccion).orElse(null);

            Log log1 = new Log();
            log1.setFechaModificacion(new Date());
            log1.setTipoAccion(ta);
            log1.setUsuarioModificacion(u);
            log1.setNombreTablaAfectada(nombreTablaAfectada);
            log1.setIdtablaAfectada(idTablaAfectada);
            log1.setRequest(request);
            log1.setEsExitosa(esTransaccionExitosa);

            logRepository.save(log1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
