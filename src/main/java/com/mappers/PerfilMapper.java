package com.mappers;

import com.dto.PerfilDTO;
import com.entities.Perfil;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PerfilMapper {
    PerfilMapper INSTANCE = Mappers.getMapper(PerfilMapper.class);

    @Mapping(source = "id", target = "id")
    PerfilDTO objectToDto(Perfil perfil);

    @Mapping(source = "id", target = "id")
    Perfil dtoToObject(PerfilDTO perfilDTO);

    List<PerfilDTO> map(List<Perfil> perfils);

    List<Perfil> mapDtos(List<PerfilDTO> perfilDTOS);

}
