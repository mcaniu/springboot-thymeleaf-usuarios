package com.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@ToString
@Getter
@Setter
public class PerfilDTO implements Serializable {

    private Integer id;

    @NotNull(message = "* El nombre no puede ir vacio")
    @NotBlank(message = "* El nombre no puede ir vacio")
    private String nombre;

    private Date fechaCreacion;
    private Date fechaModificacion;
    private String usuarioModificacion;
    private String usuarioCreacion;
    private Boolean activo;

}
