# springboot-thymeleaf-usuarios
## Proyecto que simula un mantenedor de usuarios
Proyecto que simula un mantenedor de usuarios con sus respectivas operaciones CRUD, ademas de otras funcionalidades mas.

## Futuras mejoras
- integrar vuejs en las distintas pantallas
- creacion de graficos con los usuarios con mas acciones realizadas
- creacion de pdfs con api carbone
- agregar seccion para los registros de las acciones realizadas en los registros centrales
- deteccion de error de eliminacion por conflicto claves foraneas y pop up de confirmacion de eliminacion
- pruebas unitarias

## Tecnologias aplicadas
- Java v1.8
- springboot v2.6.3
- H2DB v2.1.212(base de datos temporal)
- Spring Security v2.3.3(framework para la seguridad y autentificacion)
- Thymeleaf (motor de plantillas)
- VanillaJS(SweetAlert v2, DataTable v1.13.4, JQuery v3.5.1)
- Boostrap v5.1.3(framework para el diseño front)
