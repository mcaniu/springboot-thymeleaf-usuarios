package com.services.impl;


import com.dto.ComunaDTO;
import com.dto.RegionDTO;
import com.entities.Comuna;
import com.entities.Direccion;
import com.entities.Region;
import com.exceptions.UsuarioException;
import com.mappers.ComunaMapper;
import com.mappers.RegionMapper;
import com.repository.ComunaRepository;
import com.repository.DireccionRepository;
import com.services.ComunaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static com.utils.AppConstant.generarExceptionObjetoNulo;

@Service("comunaService1")
public class ComunaServiceImpl implements ComunaService {

    @Autowired
    ComunaRepository comunaRepository;

    @Autowired
    DireccionRepository direccionRepository;


    @Transactional
    @Override
    public Integer registrar(ComunaDTO comunaDTO) {
        Comuna comuna = ComunaMapper.INSTANCE.dtoToObject(comunaDTO);
        comuna.setRegion(RegionMapper.INSTANCE.dtoToObject(comunaDTO.getRegion()));
        // estado.setFechaCreacion(new Date());
        // estado.setUsuarioCreacion(usuarioDTO.getUsername());

        comunaRepository.save(comuna);
        return comuna.getId();
    }

    @Transactional
    @Override
    public boolean eliminar(Integer id) throws UsuarioException {
        Comuna comunaOld = comunaRepository.findById(id).orElse(null);
        generarExceptionObjetoNulo(comunaOld);

        List<Direccion> direccionesAsociadas = direccionRepository.findAllByComuna(comunaOld);
        if (direccionesAsociadas.size() > 0) {
            throw new UsuarioException("validation", "existen direcciones asociadas a la comuna");
        }

        comunaRepository.deleteComuna(id);
        return true;
    }

    @Transactional
    @Override
    public boolean actualizar(ComunaDTO comunaDTO) throws UsuarioException {
        Comuna comunaOld = comunaRepository.findById(comunaDTO.getId()).orElse(null);
        generarExceptionObjetoNulo(comunaOld);

        comunaOld.setNombre(comunaDTO.getNombre());
        comunaOld.setActivo(comunaDTO.getActivo());
        comunaRepository.save(comunaOld);

        return true;
    }

    @Override
    public ComunaDTO buscarPorId(Integer id) {
        return (id != null && id != 0) ?
                ComunaMapper.INSTANCE.objectToDto(comunaRepository.findById(id).orElse(null)) :
                null;
    }

    @Override
    public List<ComunaDTO> listar() {
        List<Comuna> comunas = comunaRepository.findAll();
        return ComunaMapper.INSTANCE.map(comunas);
    }

    @Override
    public List<ComunaDTO> listarPorRegion(RegionDTO regionDTO) {
        Region region = RegionMapper.INSTANCE.dtoToObject(regionDTO);

        List<Comuna> comunas = new ArrayList<>();
        if (region != null) {
            comunas = comunaRepository.findAllByRegion(region);
        }

        return ComunaMapper.INSTANCE.map(comunas);
    }
}
